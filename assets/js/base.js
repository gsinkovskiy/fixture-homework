const moment = require('moment');
require('moment-duration-format');
const $ = require('jquery');

$('[data-countdown-to]').each(function() {
    const el = $(this);
    const countdown = moment(el.data('countdown-to'));

    function tick() {
        const now = moment();
        const duration = moment.duration(countdown.diff(now));

        el.html(duration.format("M [months], D [days], H [hrs], m [mins], s [secs]"));

        setTimeout(tick, 1000);
    }

    tick();
})